package com.oreo.taste.jdk.eleven;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;


/**
 * GreetingController
 *
 * @author yuan
 * @since 2023/3/30 17:51
 */
@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name) {

        return "Hello, " + name;
    }

    private void test() {
        Consumer<String> consumer = t -> System.out.println(t.toUpperCase());

        BiFunction<String, Integer, String> biFunction = (t, u) -> "r" + t.toUpperCase() + u.toString();


        Consumer<String> consumer1 = (var t) -> System.out.println(t.toUpperCase());

//        错误的形式：必须要有类型，可以加上var
        Consumer<String> consumer2 = (t) -> System.out.println(t.toUpperCase());

//        正确的形式
        Consumer<String> consumer3 = (var t) -> System.out.println(t.toUpperCase());


        List<String> list = new ArrayList<>();
        list.add("yuan");
        list.add("yw");
        Object[] objects = list.toArray();
        list.toArray(new String[0]);
        String[] strings = list.toArray(String[]::new);

        int x = 1;


    }



}

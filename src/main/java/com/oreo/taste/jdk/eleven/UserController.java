package com.oreo.taste.jdk.eleven;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * UserController
 *
 * @author yuan
 * @since 2023/4/2 21:55
 */
@RestController
public class UserController {

    @GetMapping("/users")
    public List<User> list() {
        return IntStream.rangeClosed(1, 10)
                .mapToObj(i -> new User(i, "User" + i))
                .collect(Collectors.toList());
    }
}

package com.oreo.taste.jdk.eleven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TasteJdkElevenApplication {

// refer:    https://www.oracle.com/java/technologies/javase/jdk11-readme.html

//   todo: taste HttpClient
//   todo: taste G1 GC  (JDK 9/10/11/15)
//   todo: taste Epsilon GC
//-XX:+PrintGCDetails   开启详细GC日志模式
//-XX:+PrintGCDateStamps    将时间和日期也加入到GC日志中
//-XX:+UseG1GC  开启G1垃圾收集器
//-XX:+UseEpsilonGC

    public static void main(String[] args) {
        SpringApplication.run(TasteJdkElevenApplication.class, args);
    }

}
